# Draft de script de génération d'une release note à partir des commits existant entre le tag le plus récent, et celui qui le précède
# TODO : a adapter pour les répo crater/tat si on veut l'utiliser
export VERSION=$(git tag --sort=-taggerdate | head -1)
export PREVIOUS_VERSION=$(git tag --sort=-taggerdate | head -2 | awk '{split($0, tags, "\n")} END {print tags[1]}')
export CHANGES=$(git log --pretty="- %s" $VERSION...$PREVIOUS_VERSION)

echo "Génération d'une release note entre le tag $PREVIOUS_VERSION et le tag $VERSION"

printf "# Release notes (\`$VERSION\`)\n\n## Changes\n$CHANGES\n\n## Metadata\n\`\`\`\nThis version -------- $VERSION\nPrevious version ---- $PREVIOUS_VERSION\nTotal commits ------- $(echo "$CHANGES" | wc -l)\n\`\`\`\n" > release_notes.md
