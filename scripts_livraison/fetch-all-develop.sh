
# Se positionner dans le repertoire parent de tous les repos => il faudrait un CRATER_HOME
cd ../..

echo "Fetch & pull develop sur crater-data"
cd crater-data
git fetch
git checkout develop
git pull

echo "Fetch & pull develop sur crater-data-resultats"
cd ../crater-data-resultats
git fetch
git checkout develop
git pull

echo "Fetch & pull develop sur ui"
cd ../ui
git fetch
git checkout develop
git pull
