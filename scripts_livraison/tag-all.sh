
if [ $# -ne 1 ]; then
  echo 1>&2 "$0: Erreur, il faut renseigner le paramètre obligatoire => message associé au tag"
  echo 1>&2 "$0: L'id du tag est construit automatiquement à partir de la date du jour sur le modèle 'Version_2024-05-27'"
  echo 1>&2 "$0: exemple : > ./tag-all.sh 'Message long sur une seule ligne'"
  echo 1>&2 "$0: -> crée le tag avec un ID 'Version_2024-05-27'"
  exit 2
fi

# Se positionner dans le repertoire parent de tous les repos => il faudrait un CRATER_HOME
cd ../..

echo 1>&2 "# Construction du tag"
tag_id_version=$(date +%Y-%m-%d)
tag_id="Version_$tag_id_version"
tag_message=$1
echo "Valeurs id et message : tag_id=$tag_id , et tag_message=$tag_message"

read -n1 -s -r -p $'Appuyer sur espace pour démarrer la création des tags (Ctrl-C pour abandonner)\n' key

echo "Ajout du tag sur crater-data"
cd crater-data
git tag -a $tag_id -m "$tag_message"
git push origin $tag_id
git checkout develop

read -n1 -s -r -p $'Appuyer sur espace pour continuer (Ctrl-C pour abandonner)\n' key

echo "Ajout du tag sur crater-data-resultats"
cd ../crater-data-resultats
git tag -a $tag_id -m "$tag_message"
git push origin $tag_id
git checkout develop

read -n1 -s -r -p $'Appuyer sur espace pour continuer (Ctrl-C pour abandonner)\n' key

echo "Ajout du tag sur ui"
cd ../ui
git tag -a $tag_id -m "$tag_message"
git push origin $tag_id
git checkout develop