
# Se positionner dans le repertoire parent de tous les repos => il faudrait un CRATER_HOME
cd ../..

echo "Fetch & pull master sur crater-data"
cd crater-data
git fetch
git checkout master
git pull

echo "Fetch & pull master sur crater-data-resultats"
cd ../crater-data-resultats
git fetch
git checkout master
git pull

echo "Fetch & pull master sur ui"
cd ../ui
git fetch
git checkout master
git pull
