
if [ $# -ne 1 ]; then
  echo 1>&2 "$0: Erreur, merci de renseigner le paramètre obligatoire => message associé au commit"
  exit 2
fi

message_commit=$1

echo "Fetch & pull develop sur le repo courant"
git fetch
git checkout develop
git pull

echo "Pull master sur le repo courant"
git checkout master
git pull

echo "Merge develop dans master avec force commit"
git checkout master
git merge develop --no-ff -m "$message_commit"