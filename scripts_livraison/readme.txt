Mode op pour livrer une version dans master :

1) Mettre les repos locaux à jour
- se positionner dans le dossier scripts_livraison
- lancer les scripts fetch-all-develop.sh & fetch-all-master.sh pour vérifier que tous les repos locaux sont à jour (vérifer éventuels messages d'erreur et nettoyer les espaces de travail si besoin)

2) Créer les commits de merge sur master, sur les repos en local
- lancer le script fetch-merge-all-repos.sh en renseignant le nom du commit qui sera créé lors du merge (pas de ff sur master, commit explicite lors de chaque livraison)
- on utilise de préférence la convention suivante pour les messages des commits => "Version 2024-05-27"
- vérifier que tous les commits de merge sont OK

3) Pousser les commits sur le repo distant et vérifier le bon déploiement des pipelines gitlab
- pousser les commits avec push-all-master.sh
- attendre que les pipelines soient OK sur gitlab
- tester les applications

4) Créer un tag sur master
- positionner un tag avec le script tag-all.sh en donnant en paramètres id et message (Nota: date ajoutée automatiquement à l'id)
ex : sh tag-all.sh livraison "Livraison #1 novembre 2023"


RQ : En cas d'erreur, pour supprimer un tag dont l'id est "id_tag" en cas de fausse manip
- supprimer le tag en local
git tag -d id_tag
- supprimer le tag sur le serveur
git push origin :refs/tags/id_tag


