
if [ $# -ne 1 ]; then
  echo 1>&2 "$0: Erreur, merci de renseigner le paramètre obligatoire => message associé au commit, par exemple 'Version 2024-05-27'"
  exit 2
fi

message_commit=$1

# Se positionner dans le repertoire parent de tous les repos => il faudrait un CRATER_HOME
cd ../..

read -n1 -s -r -p $'Appuyer sur espace pour démarrer le merge sur crater-data (Ctrl-C pour abandonner)\n' key
cd crater-data
echo "-- MERGE SUR crater-data --"
echo "Fetch & pull develop sur le repo courant"
git fetch
git checkout develop
git pull

echo "Pull master sur le repo courant"
git checkout master
git pull

echo "Merge develop dans master avec force commit"
git checkout master
git merge develop --no-ff -m "$message_commit"

read -n1 -s -r -p $'Appuyer sur espace pour démarrer le merge sur crater-data-resultats (Ctrl-C pour abandonner)\n' key
cd ../crater-data-resultats
echo "-- MERGE SUR crater-data-resultats --"
echo "Fetch & pull develop sur le repo courant"
git fetch
git checkout develop
git pull

echo "Pull master sur le repo courant"
git checkout master
git pull

echo "Merge develop dans master avec force commit"
git checkout master
git merge develop --no-ff -m "$message_commit"



read -n1 -s -r -p $'Appuyer sur espace pour démarrer le merge sur ui (Ctrl-C pour abandonner)\n' key
cd ../ui
echo "-- MERGE SUR ui --"
echo "Fetch & pull develop sur le repo courant"
git fetch
git checkout develop
git pull

echo "Pull master sur le repo courant"
git checkout master
git pull

echo "Merge develop dans master avec force commit"
git checkout master
git merge develop --no-ff -m "$message_commit"
